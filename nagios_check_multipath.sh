
#!/bin/bash
export OUTFILE=$(mktemp /tmp/multipath.XXXXXXXXXX)
export WCOLL=/etc/dsh/group/linux.HP.blade
pdsh "/sbin/multipath -ll" | grep ' sd' | egrep -vw '(active|ready)' > "$OUTFILE"
if [ -s "$OUTFILE" ]; then
    cat "$OUTFILE" | mail -s "Multipath Problems" its.system.support.unix@umich.edu
    /usr/local/nagios/libexec/eventhandlers/submit_check_result_via_nsca booby.dsc check_multipath 2 `cat $OUTFILE`
else
    /usr/local/nagios/libexec/eventhandlers/submit_check_result_via_nsca booby.dsc check_multipath 0 ok
fi 
rm -f "$OUTFILE"
