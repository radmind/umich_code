#!/bin/bash
# check for runaway processes and presence of core files
#

#
# Boilerplate definitions for all scripts.
#
set -u		    # Undefined variables are latent bugs
TOOL=${0//*\//}     # Get the simple name of this script
TOOL_ARGS="$@"      # Save args in case other things call 'set'

#script specific variables

host=`hostname | cut -f1 -d.`
date=`date  +%D`
lsdate=`date | awk '{ print $2" "$3 }'`
corelocs='/etc/watchdog/corelocations'

notifycmd="/usr/rsug/bin/cronmail \"${host}: $TOOL report $date\""
notifycmdp="/usr/rsug/bin/cronmail \"${host}: PROCS Warning $date\""
notifywho=rdwyer@russelldwyer.com




for i in `cat $corelocs` ; do

    found_files=`ls -la $i | egrep '^[^\.]{1,}\.core\.[0-9]{1,}$' |grep "$lsdate"` 
    num_files=` echo "$found_files" | wc -l `
	if [ -n "$found_files" ]
	    then echo -en "Found "$num_files" corefiles inside of "$i"." >> /tmp/watchdogcores.$$ 
	    echo -en "\n \n Corefiles found: "$found_files"" >> /tmp/watchdogcores.$$
    	fi
done
	


if [ -e /tmp/watchdogcores.$$ ] ; then
    cat /tmp/watchdogcores.$$ | eval ${notifycmd} ${notifywho}
fi

rm -rf /tmp/watchdogcores.$$



# Processes Checker 
. /etc/watchdogscripts/config

cpucheck()
{
    ps --no-headers -eo pcpu,time,pid,ppid,user,comm | sed -e 's/\://g' | awk -v check=$1 -v time=$2 '$1 > check && $2 > time {printf "%4.1f %5d %5d %5d %8s %s\n", $1, $2, $3, $4, $5, $6}'
}

vmcheck()
{
    ps --no-headers -eo rss,time,pid,ppid,user,comm | sed -e 's/\://g' | awk -v check=$1 -v time=$2 '$1 > check && $2 > time {printf "%7s %5d %5d %5d %8s %s\n", $1, $2, $3, $4, $5, $6}'
}


###########BEGIN
# check for runaway processes
cpucheck ${maxcpu:-10} ${cputime:-100} > /tmp/psnag.$$
#cpucheck 1 1 > /tmp/psnag.$$
if [ -s /tmp/psnag.$$ ]; then
    echo "processes over maxcpu:" >> /tmp/watchdog.$$
    cat /tmp/psnag.$$ >> /tmp/watchdog.$$
    echo >> /tmp/watchdog.$$
fi

# check for processes approaching max vm limit
vmcheck ${maxvm:-1000000} ${cputime:-100} > /tmp/psnag.$$
if [ -s /tmp/psnag.$$ ]; then
    echo "processes over maxvm:" >> /tmp/watchdog.$$
    cat /tmp/psnag.$$ >> /tmp/watchdog.$$
    echo >> /tmp/watchdog.$$
fi


if [ -s /tmp/watchdog.$$ ]; then
    cat /tmp/watchdog.$$ | eval ${notifycmdp} ${notifywho}
fi

rm -f /tmp/watchdog.$$ /tmp/psnag.$$

